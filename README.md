# wx_login

#### 介绍
springboot整合微信小程序

#### 软件架构
项目描述：在微信小程序中通过与Springboot操作数据库实现登录验证，其中我是用springboot整合mybatis-plus 和mysql操作数据库

主要技术：SpringBoot、lombok、mybatis-plus、mysql 、微信小程序

前端地址：https://gitee.com/ckfeng/applet_of_wechat

博客地址：https://www.cnblogs.com/ckfeng/p/12812214.html

效果图如下：
### 登录页面
![登录页面](https://images.gitee.com/uploads/images/2020/0501/013210_3df95164_5420874.png "11.png")
### 首页
![首页](https://images.gitee.com/uploads/images/2020/0501/013305_d5d9b8bb_5420874.png "22.png")

### 添加页面
![添加页面](https://images.gitee.com/uploads/images/2020/0501/013353_d5f88626_5420874.png "44.png")

### 修改页面
![修改页面](https://images.gitee.com/uploads/images/2020/0501/013444_7a63ccd5_5420874.png "33.png")

### 删除页面
![删除页面](https://images.gitee.com/uploads/images/2020/0501/013513_f9275ad6_5420874.png "55.png")
