package com.ckf.login_wx.servic;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ckf.login_wx.entity.User;

/**
 * @author 安详的苦丁茶
 * @date 2020/4/30 14:07
 */
public interface UserService extends IService<User> {
}
