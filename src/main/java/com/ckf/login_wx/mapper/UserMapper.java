package com.ckf.login_wx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ckf.login_wx.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author 安详的苦丁茶
 * @date 2020/4/30 14:07
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
