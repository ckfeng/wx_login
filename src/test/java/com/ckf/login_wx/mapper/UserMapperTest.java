package com.ckf.login_wx.mapper;

import com.ckf.login_wx.entity.User;
import com.ckf.login_wx.servic.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author 安详的苦丁茶
 * @date 2020/4/30 14:22
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserService userService;

    @Test
    public void list(){
        List<User> list=userService.list();
        for (User user : list) {
            System.out.println(user);
        }
    }
}