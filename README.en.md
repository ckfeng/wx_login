# wx_login

#### Description
springboot整合微信小程序实现简单的登录与简单的增删盖茶

项目描述：在微信小程序中通过与Springboot操作数据库实现登录验证，其中我是用springboot整合mybatis-plus 和mysql操作数据库
主要技术：SpringBoot、lombok、mybatis-plus、mysql 、微信小程序

效果图如下

### 登录页面](https://images.gitee.com/uploads/images/2020/0501/011945_5fbe3e2a_5420874.png "11.png "登录页面.png")

